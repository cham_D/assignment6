#include <stdio.h>

int fibonacciSeq(int n){
	if(n==1){
		return 0;
	}
	if(n>=4){
		return  fibonacciSeq(n-1) + fibonacciSeq(n-2);
	}
	else{
		return 1;
	}
}


int main(){
	int n, n1;
	printf("Enter: ");
	scanf("%d", &n);
	for(n1=1; n1<=n; n1++){
		printf("%d \n", fibonacciSeq(n1));
	}
	return 0;
}
